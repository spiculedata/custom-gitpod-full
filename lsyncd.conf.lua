settings {
   logfile = "/tmp/lsyncd.log",
   statusFile = "/tmp/lsyncd.status",
   statusInterval = 20,
   nodaemon   = false
}

sync {
        default.rsyncssh,
        source = "/data",
        host = "sshplaceholder",
        targetdir = "targetplaceholder",
}
