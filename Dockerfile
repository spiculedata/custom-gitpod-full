FROM gitpod/workspace-full

USER root

#ADD onboarding.sh /

#RUN add-apt-repository -y ppa:git-core/ppa \
#    && install-packages git git-lfs openjdk-8-jdk openjdk-11-jdk sshfs lsyncd && chmod +x /onboarding.sh

RUN sudo sh -c '(echo "#!/usr/bin/env sh" && curl -L https://github.com/lihaoyi/Ammonite/releases/download/2.0.4/2.13-2.0.4) > /usr/local/bin/amm && chmod +x /usr/local/bin/amm' && brew install scala && brew install coursier/formulas/coursier sbt scalaenv && sudo env "PATH=$PATH" coursier bootstrap org.scalameta:scalafmt-cli_2.12:2.4.2   -r sonatype:snapshots   -o /usr/local/bin/scalafmt --standalone --main org.scalafmt.cli.Cli && scalaenv install scala-2.12.11 && scalaenv global scala-2.12.11 
#ADD fuse.conf /etc/fuse.conf
#ADD lsyncd.conf.lua /etc/lsyncd/lsyncd.conf.lua
ADD sshdconf /etc/ssh/ssh_config
USER gitpod
