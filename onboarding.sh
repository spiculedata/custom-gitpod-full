#!/bin/bash

WORKSPACE=$1
echo -e $SSH_KEY > ~/.ssh/id_ed25519
chmod 600 ~/.ssh/id_ed25519
ssh $SSH_CONNECTION "mkdir -p /data/'$GITPOD_GIT_USER_NAME$GITPOD_REPO_ROOT'"
sudo mkdir -p /data
sudo chown -R gitpod:gitpod /data/
rsync -chavzP --stats $SSH_CONNECTION:"'/data/$GITPOD_GIT_USER_NAME/$GITPOD_REPO_ROOT'/" /data || true
sudo sed -i -e "s|targetplaceholder|/data/$GITPOD_GIT_USER_NAME$GITPOD_REPO_ROOT|g" /etc/lsyncd/lsyncd.conf.lua
sudo sed -i -e "s|sshplaceholder|$SSH_CONNECTION|g" /etc/lsyncd/lsyncd.conf.lua
lsyncd /etc/lsyncd/lsyncd.conf.lua
